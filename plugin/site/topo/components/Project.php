<?php namespace Site\Topo\Components;

use Cms\Classes\ComponentBase;
use Site\Topo\Models\Portifolio as PortifolioModel;

class Project extends ComponentBase
{

    public $portifolio;

    public function componentDetails()
    {
        return [
            'name'        => 'Project Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'project_id' => [
                 'title'             => 'Id do projeto',
                 'description'       => 'Id para encontrar projeto correto e renderizar',
                 'default'           => 0,
                 'type'              => 'integer',
                 'validationPattern' => '[0-9]+$',
                 'validationMessage' => 'Por favor passe um numero valido'
            ]
        ];
    }

    public function onRun()
    {
        $this->portifolio = PortifolioModel::findOrFail($this->property('project_id'));
    }
}